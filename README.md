# Lawn And Garden - One Page - CDN


## Description
A one page responive website using html and some javascript. Some nice animation is included too.



## Support
For support, bug reports, suggestions and more visit the [Issues Area](../../issues).

## Roadmap
Follow [the ROADMAP](../../milestones) as the future of this project gets planned. 

## License
Read the [LICENSE](LICENSE) details.

## Project status
Active


<!-- ADD BEFORE SUPPORT
 ## Badges


## Visuals


## Installation


## Usage
-->

<!-- ADD BETWEEN ROADMAP & LICENSE
## Contributing
## Authors and acknowledgment
-->